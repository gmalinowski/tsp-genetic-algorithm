package com.tsp;

/**
 * Created by malin on 20.10.16.
 */
import java.util.ArrayList;
import java.util.Collections;

public class Tour {
    ////////////////////////STATYCZNE////////////////////////////////
    ///////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////
    private static ArrayList citiesArr = new ArrayList<City>();
    private static int id = 0;

    static void addCity() {
        City city = new City();
        citiesArr.add(city);
    }
    static void addCity(int x, int y) {
        City city = new City(x, y);
        citiesArr.add(city);
    }
    static int getNumberOfCities() {
        return citiesArr.size();
    }
    static int getID() {
        id++;
        return id;
    }
    /////////////DEEP COPYING//////////////////////////
    public static ArrayList<City> cloneArrayList(ArrayList<City> lista) {
        ArrayList<City> clone = new ArrayList<City>(lista.size());
        for (City item : lista) {
            clone.add(new City(item));
        }
        return clone;
    }
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////

    private ArrayList tourArr = new ArrayList<City>();
    double distance;

    Tour() {
        for (int i = 0; i < getNumberOfCities(); i++) {
            tourArr.add(null);
        }
    }

    void generateTour() {
        tourArr = cloneArrayList(citiesArr);
        Collections.shuffle(tourArr);
    }

    void addCityInCerteinPosition(int position, City city) {
        tourArr.set(position, city);
        distance = 0;
    }

    void swapCity(int i, int j) {
        Collections.swap(tourArr, i, j);
    }

    City getCity(int position) {
        return (City) tourArr.get(position);
    }

    double getTourDistance() {

        double suma = 0;
        City c1, c2;

        for (int i = 0; i < tourArr.size() - 1; i++) {
            c1 = (City)tourArr.get(i);
            c2 = (City)tourArr.get(i+1);

            suma += c1.distanceToCity(c2);
        }
        c2 = (City)tourArr.get(tourArr.size()-1);
        suma += c2.distanceToCity((City)tourArr.get(0));
        distance = suma;
        return suma;
    }




    boolean containCity(City city) {

        for (int i = 0; i < getNumberOfCities(); i++) {

if (this.getCity(i) != null) {

    if (city.cityID == this.getCity(i).cityID) {
        return true;
    }
}
        }
        return false;
//        return tourArr.contains(city);
    }


    @Override
    public String toString() {
        String str = new String();
        for (int i = 0; i < getNumberOfCities(); i++) {
            if (i == getNumberOfCities() - 1) {
                str += getCity(i);
            } else {
                str += getCity(i) + "\n";
            }
        }
        return str;
    }
}
