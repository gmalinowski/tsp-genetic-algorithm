package com.tsp;



/**
 * Created by malin on 20.10.16.
 */
public class Algorithm {
    static double mutRatio = 0.015;
    static int tournamentS = 5;

    static void setMutationRatio(double mutationRatio) {
        mutRatio = mutationRatio;
    }
    static void setTournamentSize(int tournamentSize) {
        tournamentS = tournamentSize;
    }
//////////////////////////////EVOLUTION/////////////////////////////////
    static Population evolution(Population popEv) {
        //for (int j = 0; j < evoTime; j++) {
            Population newPop = new Population(popEv.getPopSize(), false);

            for (int i = 0; i < newPop.getPopSize(); i++) {
                Tour t1 = tournamentSelection(popEv);
                Tour t2 = tournamentSelection(popEv);

                Tour child = crossover(t1, t2);


                newPop.setTourbyIndex(i, child);

            }
            for (int i = 0; i < newPop.getPopSize(); i++) {
                mutation(newPop.getTourbyIndex(i));
            }
        return newPop;
    }
///////////////////////////////KRZYZOWANIE//////////////////////////////////
    private static Tour crossover(Tour parent1, Tour parent2) {
        Tour child = new Tour();

        int start, stop;

        do {
            start = (int) (Math.random() * (parent1.getNumberOfCities()));
            stop = (int) (Math.random() * (parent2.getNumberOfCities()));


            if (stop < start) {
                int tmp = stop;
                stop = start;
                start = tmp;
            }

        } while (start == stop && Tour.getNumberOfCities() > 1);


        for (int i = 0; i < child.getNumberOfCities(); i++) {
            if ((i < start || i > stop) ) {
                child.addCityInCerteinPosition(i, parent1.getCity(i));
            }
        }

        for (int i = 0; (i < child.getNumberOfCities()) && (start <= stop); i++) {
            if (!child.containCity(parent2.getCity(i))) {
                    child.addCityInCerteinPosition(start, parent2.getCity(i));
                start++;
            }
        }
        return child;
    }


/////////////////////////////MUTACJA///////////////////////////////
    private static void mutation(Tour tour) {
        for (int i = 0; i < tour.getNumberOfCities(); i++) {
            int j;
            if (Math.random() < mutRatio) {
                do {
                    j = (int) (Math.random() * tour.getNumberOfCities());
                }while (i == j && Tour.getNumberOfCities() > 1);

                tour.swapCity(i, j);
            }
        }
    }
////////////////////////TURNIEJ - SELEKCJA////////////////////////////////////////////////////////
    private static Tour tournamentSelection(Population pop) {
        Population tournamentPop = new Population(tournamentS, false);

        for (int i = 0; i < tournamentS; i++) {
            int randomTour = (int) (Math.random() * pop.getPopSize());
            tournamentPop.setTourbyIndex(i, pop.getTourbyIndex(randomTour));
        }
        return tournamentPop.getBestTour();
    }
}
