package com.tsp;

/**
 * Created by malin on 20.10.16.
 */
public class Population {
    Tour [] populationTab;
    int popSize;

    Population(int populationSize, boolean initialize) {
        populationTab = new Tour [populationSize];
        popSize = populationSize;
        if(initialize) {
            for (int i = 0; i < populationSize; i++) {
                populationTab[i] = new Tour();
                populationTab[i].generateTour();
            }
        }
    }

    Tour getBestTour() {
        Tour tmp = populationTab[0];
        for (int i = 1; i < popSize; i++) {
            if (populationTab[i].getTourDistance() < tmp.getTourDistance()) {
                tmp = populationTab[i];
            }
        }
        return tmp;
    }
    Tour getWorstTour() {
        Tour tmp = populationTab[0];
        for (int i = 1; i < popSize; i++) {
            if (populationTab[i].getTourDistance() > tmp.getTourDistance()) {
                tmp = populationTab[i];
            }
        }
        return tmp;
    }
    Tour getTourbyIndex(int index) {
        return populationTab[index];
    }
    void setTourbyIndex(int index, Tour tour) {
        populationTab[index] = tour;
    }

    int getPopSize() {
        return popSize;
    }

}
