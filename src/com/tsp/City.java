package com.tsp;

/**
 * Created by malin on 20.10.16.
 */
public class City {
    int x, y, cityID;
    static int randomCityPosition = 101;

    City() {
        this.x = (int)(Math.random()*randomCityPosition);
        this.y = (int)(Math.random()*randomCityPosition);
        cityID = Tour.getID();
    }
    City(int x, int y) {
        this.x = x;
        this.y = y;
        cityID = Tour.getID();
    }
    City(City city) {
        this.x = city.x;
        this.y = city.y;
        this.cityID = city.getID();
    }
    static void setRandomCityPosition(int cityPosition) {
        randomCityPosition = cityPosition;
    }
    boolean compare(City city) {
        if (
                this.x == city.x &&
                this.y == city.y &&
                        this.cityID == city.getID()
                ) {
            return true;
        }
        return false;
    }
    int getX() {
        return x;
    }
    int getY() {
        return y;
    }
    int getID() { return cityID;}
    double distanceToCity(City city) {
        int xD = Math.abs(this.x - city.getX());
        int yD = Math.abs(this.y - city.getY());

        return Math.sqrt((xD*xD)+(yD*yD));
    }//kom

    @Override
    public String toString() {
        String str = cityID + ". (" + x + ", " + y +")";
        return str;
    }
}
