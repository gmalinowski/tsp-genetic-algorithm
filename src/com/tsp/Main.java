package com.tsp;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main extends JFrame implements ActionListener{
    JButton BDalej;
    JLabel liloscMiast;
    JTextField tiloscMiast;
    Main() {
        setSize(800, 600);
        setTitle("Travelling Salesman Problem - Genetic Algorithm");

        setLayout(null);
        BDalej = new JButton("Start");
        BDalej.setBounds(300, 500, 200, 50);
        add(BDalej);
        BDalej.addActionListener(this);

        liloscMiast = new JLabel("Podaj liczbę miasts: ");
        liloscMiast.setBounds(300, 200, 200, 50);
        add(liloscMiast);

        tiloscMiast = new JTextField("");
        tiloscMiast.setBounds(350, 250, 100, 25);
        add(tiloscMiast);
    }



    public static void main(String[] args) {


        Main window = new Main();
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setVisible(true);


	    //WCZYTYWANIE MIAST
        //GENEROWANIE POPULACJI
        //EWOLUCJA
        //population size

        Algorithm.setMutationRatio(0.015);
        Algorithm.setTournamentSize(5);
        City.setRandomCityPosition(100);

        Tour.addCity(1,1);
        Tour.addCity(7,1);
        Tour.addCity(3,5);
        Tour.addCity(2,9);
        Tour.addCity(8,7);
        Tour.addCity(9,5);
        Tour.addCity(93,34);
        Tour.addCity(3,34);
        Tour.addCity(3,3);
        Tour.addCity(13,31);
        Tour.addCity(2,4);
        Tour.addCity(39,19);



        Population pop = new Population(100, true);
        System.out.println("Trasa poczatkowa: " + pop.getBestTour().getTourDistance());
        pop = Algorithm.evolution(pop);
        for (int i = 0; i < 50; i++) {
            pop = Algorithm.evolution(pop);
        }
        System.out.println("Trasa koncowa: " + pop.getBestTour().getTourDistance());





        //System.out.println("Trasa najgorsz: " + pop.getWorstTour().getTourDistance());
        System.out.println(pop.getBestTour());

        //for (int i = 0; i < 10; i++) {
          //  pop = Algorithm.evolution(pop);
        //}
        //System.out.println("Trasa końcowa: " + pop.getBest().getTourDistance());
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        Object sour = e.getSource();
        if (sour == BDalej) {
            //lwynik.setText(String.valueOf(wynik));
        }
    }
}
